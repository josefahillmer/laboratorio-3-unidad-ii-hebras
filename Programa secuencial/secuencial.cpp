/*
Programa que recibe a través de sus argumentos una lista de archivos y cuente
cuantas lineas, palabras y caracteres hay en cada archivo,
asi como el total entre todos los archivos
 */

// Librerias
#include <ctime>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <iostream>
#include <sstream>

using namespace std;

// Procedimiento que busca las letras
void abrir(void *archivos, int &total_l, int &total_p, int &total_c){
    // Variable que recorre el archivo
    char *str;
    // Es el caracter en el que esta ese momento
    char c[2];
    // Contadores de las variable para caracter, lineas y palabras
    int caracter = 0;
    int lineas = 0;
    int palabras = 0;

    // Asignación
    str = (char *) archivos;

    // Se abre el archivo
    int stream = open(str, O_RDONLY);

    // Cuando no se puede abrir el archivo
    if(stream == -1){
      cout << "Error al abrir los archivos" << endl;
      exit(0);
    }

    // Se puede abrir el archivo por lo que cuenta lo que se pide
    while(read(stream, c, 1)){
      /* Se cuenta la cantidad de caracteres */
      if (strchr(str,c[1])) {
        caracter++;
      }
      // Se cuenta la cantidad de lineas
      if(strchr("\n", c[0])){
        lineas++;
      }
      // Se cuenta la cantidad de palabras, estás son  separados por espacios
      if(strchr(" ", c[0])){
        palabras++;
      }
    }
    close(stream);

    /* Las palabras se suman a las lineas ya que antes de cada salto de linea
    hay una palabra */
    palabras = palabras + lineas;

    // Se muestra la cantidad de todo lo que se pide
    cout << "\nArchivo: " << (string) str << endl;
    cout << "Lineas: " << lineas << endl;
    cout << "Palabras: " << palabras << endl;
    cout << "Caracteres: " << caracter << endl;

    total_l += lineas;
    total_c += caracter;
    total_p += palabras;
}

// Se calculo el tiempo que toma el proceso
float calcularTiempo(float tiempoInicial, float tiempoFinal){
    float tiempoEjecucion = (tiempoFinal - tiempoInicial)/CLOCKS_PER_SEC;

    return tiempoEjecucion;
}

// Procedimiento principal
int main(int argc, char* argv[]) {
  // Variables que cuento el total
  int total_l = 0;
  int total_p = 0;
  int total_c = 0;

  // Variable tiempo
  float tiempoInicial, tiempoFinal;
  // Cantidad de archivos que se desean entrar
  int cantidad = argc - 1;
  // Se crea una lista con los archivos
  char* archivos[cantidad];

  // Comprobamos que el usuario entregue un archivo de texto
  if(argc <= 1){
    cout << "Ingrese un archivo de texto" << endl;
  }
  tiempoInicial = clock();
  // Se rellena la lista con los archivos
  for(int i=0; i < cantidad; i++){
    archivos[i] = argv[i+1];
    abrir(archivos[i], total_l, total_p, total_c);
  }
  tiempoFinal = clock();

  cout << "\nTotal de líneas, palabras y caracteres de los archivos: " << endl;
  cout  << "Líneas: " << total_l << endl;
  cout << "Palabras: "  << total_p << endl;
  cout << "Caracteres: " << total_c  << endl;
  cout << "Tiempo: " << calcularTiempo(tiempoInicial,tiempoFinal) << " segundos" << endl;

  return 0;
}
