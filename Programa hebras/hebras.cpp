/*
Programa que recibe a través de sus argumentos una lista de archivos y cuente
cuantas lineas, palabras y caracteres hay en cada archivo,
asi como el total entre todos los archivos
 */

// Librerias
#include <ctime>
#include <pthread.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <iostream>
#include <sstream>

using namespace std;

// Estructura que nos permitira sacar el total de los datos
struct hebras_struc{
    char *id;
    int lineas;
    int palabras;
    int caracter;
};

// Procedimiento que busca los datos
void *abrir(void *archivos){
    // Se llama la Estructura
    hebras_struc *datos;
    // Es el caracter en el que esta ese momento
    char c[2];
    // Contadores de las variable para caracter, lineas y palabras
    int caracter = 0;
    int lineas = 0;
    int palabras = 0;

    // Asignación
    datos = (hebras_struc *) archivos;

    // Se abre el archivo
    int stream = open(datos->id, O_RDONLY);

    // Cuando no se puede abrir el archivo
    if(stream == -1){
      cout << "Error al abrir los archivos" << endl;
      pthread_exit(0);
    }

    // Se puede abrir el archivo por lo que cuenta lo que se pide
    while(read(stream, c, 1)){
      /* Se cuenta la cantidad de caracteres */
      caracter++;
      // Se cuenta la cantidad de lineas
      if(strchr("\n", c[0])){
        lineas++;
      }
      // Se cuenta la cantidad de palabras, estás son  separados por espacios
      if(strchr(" ", c[0])){
        palabras++;
      }
    }

    /* Las palabras se suman a las lineas ya que antes de cada salto de linea
    hay una palabra */
    palabras = palabras + lineas;

    // Se muestra la cantidad de todo lo que se pide
    cout << "\nArchivo: " << datos->id << endl;
    cout << "Lineas: " << lineas << endl;
    cout << "Palabras: " << palabras << endl;
    cout << "Caracteres: " << caracter << endl;

    close(stream);

    datos->lineas = lineas;
    datos->palabras = palabras;
    datos->caracter = caracter;

    pthread_exit(0);
}

// Se calculo el tiempo que toma el proceso
float calcularTiempo(float tiempoInicial, float tiempoFinal){
    float tiempoEjecucion = (tiempoFinal - tiempoInicial)/CLOCKS_PER_SEC;
    return tiempoEjecucion;
}

int main(int argc, char* argv[]) {
  // Variables que cuento el total
  int total_l = 0;
  int total_p = 0;
  int total_c = 0;

  pthread_t threads[argc - 1];
  // Se crea una estructura con una lista que luego se ira rellenando con los archivos
  hebras_struc contenido[argc - 1] = {};

  // Variable tiempo
  float tiempoInicial, tiempoFinal;
  tiempoInicial = clock();

  // Crea todos los hilos
  for(int i=0; i < argc - 1; i++){
    hebras_struc archivo;
    // Se guarda en la lista los archivos
    contenido[i] = archivo;
    // Se sacan los nombres de los archivos
    contenido[i].id = argv[i+1];
    pthread_create(&threads[i], NULL, abrir, (void *)&contenido[i]);
  }
  /* Para esperar por el término de todos los hilos */
  for (int i=0; i< argc - 1; i++) {
    pthread_join(threads[i], NULL);
    // Se obtienen los datos totales cuando terminan los hilos
    total_l = total_l + contenido[i].lineas;
    total_p = total_p + contenido[i].palabras;
    total_c = total_c + contenido[i].caracter;
  }
  tiempoFinal = clock();

  cout << "\nTotal de líneas, palabras y caracteres de los archivos: " << endl;
  cout  << "Líneas: " << total_l << endl;
  cout << "Palabras: "  << total_p << endl;
  cout << "Caracteres: " << total_c  << endl;
  cout << "Tiempo: " << calcularTiempo(tiempoInicial,tiempoFinal) << " segundos" << endl;

  return 0;
}
