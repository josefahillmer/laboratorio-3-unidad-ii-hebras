# Laboratorio 3 - Unidad II 
**Módulo Sistemas Operativos y Redes - Programa hebras**

Programa hebras:
Realiza un programa que recibe a través de sus argumentos una lista de archivos y cuente cuantas lı́neas, palabras y caracteres hay en cada archivo, ası́ como el total entre todos los archivos.En este se crea un thread por cada archivo a contar, mediante la realización de hebras (hilos), que nos permitira realizar tareas en paralelo. Y por último a modo de comparación entre ambos programas se realiza el cálculo del tiempo de ejecución. 

# Empezando
 
Se deben abrir el archivo por una terminar desde la localizacion de la carpeta correspondiente.
Se pidira ejecutar el programa mediante un parámetro de entrada (archivo txt), cuando se ejecute se mostrará los archivos que se analizaron con su cantidad correspondiente de líneas, palabras y caracteres y por último se mostrará el total de cada uno con el tiempo que se demoro en realizar el proceso. 
Al momento de no ejecutar el programa con un paramentro mostrara un error y recuerde que el archivo que desee analizar debe estar en la carpeta correspondiente del programa para poder reconocerlo, si no esta en la carpeta, mostrará un error. 

# Prerequisitos

- Sistema operativo linux:
De preferencia Ubuntu o Debian

- Lenguaje C+:
Este lenguaje viene por defecto. Donde se utilizara .cpp

- Editor de texto

- Make:
Se necesita instalar un make para compilar el programa de manera sencilla. Se instala mediante el siguiente comando:

`sudo apt install make`

Al tener instalado el make solo se debe ejecutar en la terminal el comando make dentro de la carpeta con los archivos para crear el programa.


# Ejecutando las pruebas

Para ejecutar el programa **hebras** se debe realizar los siguientes comandos en la terminal en la carpeta correspondiente:

`g++ -o hebras hebras.cpp -lpthread`

`make`

`./hebras {palabras.txt frases.txt}` 

Se debe inicial el programa mediante un parámetro de entrado: que puede ser cualquier archivo txt que se encuentre en la carpeta donde está el programa a ejecutar. Si se ingresa más de un archivo estos se deben separar con un espacio.

Primero se creara una estructura para cada uno de los archivos que ingrese el usuario, guardando el nombre del archivo, lineas, palabras y caracteres. Luego se invia a una función que realizara todos los calculos necesarios para calcular los datos. Se recorrera el archivo caracter a caracter y bajo ciertas condiciones se podra sacar el total de palabras (la condición de " ", ya que por cada espacio que hay habra una palabra) y el total de lineas (la condición de "\n", ya que por cada salto de línea hay una). Después se mostrará el total de líneas, palabras y caracteres de cada archivo, y por último se sumara cada uno de estos y se mostrará el total que hay con todos los archivos que se ingresaron. 
También hay una función que medira el tiempo en que se demora en realiar el proceso. 

Ejemplo de como se mostrará el programa luego de su ejecución:


        Archivo: archivo1.txt
        Lineas: 3
        Palabras: 69
        Caracteres: 382

        Archivo: archivo2.txt
        Lineas: 2
        Palabras: 10 
        Caracteres: 40

        Total de líneas, palabras y caracteres de los archivos: 
        Líneas: 5
        Palabras: 79
        Caracteres: 422
        Tiempo: 0.000755 segundos

Cuando termine de analizar los archivos, el programa terminará. 

# Despliegue
La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta. Se debe de ejecutar bajo el sistema operativo Linux.

# Construido con
- Ubuntu: Sistema operativo.
- C++ : Es un lenguaje de programación
- Librerias: ctime, pthread.h, unistd.h, fcntl.h, tring.h, iostream, sstream

# Autores
Josefa Hillmer - jhillmer19@alumnos.utalca.cl
